const tplString = `
<style>
div {
  width: 100vw;
  height: 100vh;
  position: fixed;
  left: 0;
  top: 0;
  background: rgba(0, 0, 0, .3);
}
</style>
<div><slot></slot></div>
`

export default class AxMask extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-mask', AxMask)
