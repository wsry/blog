const tplString = `
  <style>
  a {
    text-decoration-line: underline;
    color: #000000;
  }
  </style>
  <a><slot></slot></a>
`

export default class AxA extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    const content = tpl.content
    content.querySelector('a').setAttribute('href', this.getAttribute('href'))
    shadow.appendChild(content.cloneNode(true))
  }
}

customElements.define('ax-a', AxA)
