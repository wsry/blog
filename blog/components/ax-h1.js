const tplString = `
<style>
h1 {
  margin: 0 0 20px;
  font-weight: 600;
  font-size: 28px;
  line-height: 72px;
}
</style>
<h1><slot></slot></h1>
`

export default class AxH1 extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-h1', AxH1)
