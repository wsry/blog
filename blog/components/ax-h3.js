const tplString = `
<style>
h3 {
  font-weight: 600;
  font-size: 18px;
  line-height: 46px;
  margin: 0 0 12px;
}
</style>
<h3><slot></slot></h3>
`

export default class AxH3 extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-h3', AxH3)
