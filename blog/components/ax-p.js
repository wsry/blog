const tplString = `
<style>
p {
  font-size: 16px;
  line-height: 24px;
  text-indent: 32px;
  margin: 0 0 20px;
  color: #4F4F4F;
}
</style>
<p><slot></slot></p>
`

export default class AxP extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-p', AxP)
