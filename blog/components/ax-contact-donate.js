import './ax-h2.js'
import './ax-p.js'
import './ax-a.js'

const tplString = `
<style>
div {
  margin-top: 20px;
  background-color: GAINSBORO;
  padding: 1rem;
  border-radius: .5rem;
}
div section:nth-child(2) img {
  width: 100px;
  height: 100px;
  margin-top: 10px;
  margin-left: 1rem;
}
div a {
  margin: 0 6px;
}
div section:nth-child(1) img {
  width: 16px;
  height: 16px;
  vertical-align: text-top;
  margin-right: 4px;
}
</style>
<div>
  <section>
    <ax-h2>评论</ax-h2>
    <ax-p>可以，但没必要。估计也没啥评论，搞得不好网站说不定还被封。真有不解之处可以微博<ax-a href="https://weibo.com/u/2970221897?refer_flag=1001030103_&is_hot=1"><img src="http://blog-1253688605.cos.ap-shanghai.myqcloud.com/weibo.svg" alt="weibo">希饭配粥</ax-a>私信我。</ax-p>
  </section>
  <section>
    <ax-h2>打赏</ax-h2>
    <ax-p>觉得有帮助可以打赏喝杯茶，五块十块不嫌多，一毛两毛也是爱</ax-p>
    <img src="http://blog-1253688605.cos.ap-shanghai.myqcloud.com/qrcode.png" alt="qrcode">
  </section>
</div>
`

export default class AxContactDonate extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-contact-donate', AxContactDonate)