import './ax-mask.js'

const tplString = `
<style>
img {
  width: 100%;
}
.detail {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 90%;
}
</style>
<img />
<ax-mask style="display: none">
  <img class="detail" />
</ax-mask>
`

export default class AxImg extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    const content = tpl.content
    content.querySelectorAll('img').forEach(v => v.setAttribute('src', this.getAttribute('src')))
    shadow.appendChild(content.cloneNode(true))
  }

  connectedCallback () {
    this.shadowRoot.querySelector('ax-mask').addEventListener('click', () => {
      this.shadowRoot.querySelector('ax-mask').style.display = 'none'
    })
    this.shadowRoot.querySelector('img').addEventListener('click', () => {
      this.shadowRoot.querySelector('ax-mask').style.display = 'block'
    })
  }
}

customElements.define('ax-img', AxImg)
