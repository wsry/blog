const tplString = `
<style>
h2 {
  font-weight: 600;
  font-size: 20px;
  line-height: 46px;
  margin: 0 0 12px;
}
</style>
<h2><slot></slot></h2>
`

export default class AxH2 extends HTMLElement {
  constructor () {
    super()
    const shadow = this.attachShadow({ mode: 'open' })
    const tpl = document.createElement('template')
    tpl.innerHTML = tplString
    shadow.appendChild(tpl.content.cloneNode(true))
  }
}

customElements.define('ax-h2', AxH2)
