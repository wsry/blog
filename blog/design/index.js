const buttons = document.querySelectorAll('.ax-button')

for (const button of buttons) {
  button.addEventListener('mousedown', () => {
    if (button.dataset.type !== 'disabled') {
      button.classList.add('ax-button-press')
    }
  }, false)
  button.addEventListener('mouseout', () => {
    button.classList.remove('ax-button-press')
  }, false)
  button.addEventListener('mouseup', () => {
    button.classList.remove('ax-button-press')
  }, false)
  button.addEventListener('touchstart', () => {
    if (button.dataset.type !== 'disabled') {
      button.classList.add('ax-button-press')
    }
  }, false)
  button.addEventListener('touchend', () => {
    button.classList.remove('ax-button-press')
  }, false)
}