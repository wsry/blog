const data = [
  '{"question":"1192年和萨拉丁举行和谈，并签订了休战协定的统帅是","answer":"理查一世"}',
  '{"question":"1682年，法国国王路易十四宣布将宫廷从巴黎卢浮宫迁往哪个王宫","answer":"凡尔赛宫"}',
  '{"question":"1688年的哪次革命确立了英国君主立宪制","answer":"光荣革命"}',
  '{"question":"17世纪至19世纪普鲁士王国统领地区主要是现在的哪个国家","answer":"德国"}',
  '{"question":"1851年在英国伦敦召开的首届万国工业博览会的主要展示馆叫什么","answer":"水晶宫"}',
  '{"question":"19世纪爆发的卡洛斯战争是哪个国家的内战","answer":"西班牙"}',
  '{"question":"2000年夏季奥运会的主办城市是","answer":"悉尼"}',
  '{"question":"21世纪谁攻陷君士坦丁堡","answer":"莫罕默德二世"}',
  '{"question":"“古希腊三贤” 是指柏拉图，亚里士多德还有谁","answer":"苏格拉底"}',
  '{"question":"“奥古斯都”是哪位罗马帝国君主在平息罗马内战后的封号","answer":"盖乌斯·屋大维"}',
  '{"question":"“安息古国”遗址现在位于亚洲哪个国家境内","answer":"伊朗"}',
  '{"question":"《孙子兵法》的作者是","answer":"孙武"}',
  '{"question":"一神论者是指","answer":"崇拜一个神明"}',
  '{"question":"一般认为，人类的祖先最早生活在哪个大洲","answer":"非洲"}',
  '{"question":"下列古希腊哲学家中率先参与提出原子论的是哪位哲学家","answer":"德谟克里特"}',
  '{"question":"下列哪一个国家相对距离北极点更近","answer":"芬兰"}',
  '{"question":"下列哪一种猫咪因为无毛而著名","answer":"斯芬克斯猫"}',
  '{"question":"下列哪一项不属于在酒馆的白银宝箱中可能获得的奖励？","answer":"等级3知识之书"}',
  '{"question":"下列哪一项不是统帅佩拉约拥有的被动技能","answer":"冲锋"}',
  '{"question":"下列哪一项为相对古老的海洋动物","answer":"鹦鹉螺"}',
  '{"question":"下列哪个城市在1900年举办过奥运","answer":"巴黎"}',
  '{"question":"与百济、新罗合成\\"朝鲜三国时代\\"的是古代哪个民族政权","answer":"高句丽"}',
  '{"question":"世界上唯一一个诞生于热带丛林而不是大河流域的古代文明是","answer":"印加文明"}',
  '{"question":"世界上最大的岛屿是","answer":"格陵兰岛"}',
  '{"question":"世界上最早的香水是哪个国家的人发明的","answer":"塞浦路斯"}',
  '{"question":"世界上第一个付费公厕出现在哪个古代国家","answer":"古罗马"}',
  '{"question":"世界上第一部完整的成文法典是古巴比伦的哪部法典","answer":"《汉谟拉比法典》"}',
  '{"question":"世界四大音乐剧包括猫、西贡小姐、悲惨世界和哪部音乐剧","answer":"歌剧魅影"}',
  '{"question":"世界无烟日","answer":"0531"}',
  '{"question":"世界最长河流","answer":"尼罗河"}',
  '{"question":"东罗马帝国又称为什么帝国","answer":"拜占庭"}',
  '{"question":"中国古代四大名琴是号钟、绿绮、焦尾和什么","answer":"绕梁"}',
  '{"question":"中国四大发明不包括以下哪项","answer":"地动仪"}',
  '{"question":"为了正义目标可以不择手段是君主论作者的观点，他是谁","answer":"马基雅维利"}',
  '{"question":"为了治好王妃的思乡病，巴比伦国王修建了哪个世界奇迹","answer":"空中花园"}',
  '{"question":"为什么有利特征更容易遗传给后代","answer":"因为它们更利于生息繁衍"}',
  '{"question":"为马丁路德提供食物和住所的行为触犯了哪条教会官方手段","answer":"沃木斯谕旨"}',
  '{"question":"乱世之枭雄","answer":"曹操"}',
  '{"question":"人体含水量最高的器官是","answer":"眼球"}',
  '{"question":"人体缺少哪种元素会有机率引发甲状腺肿大","answer":"碘"}',
  '{"question":"从欧洲绕好望角到印度航海路线的开拓者是哪位葡萄牙航海家","answer":"达伽马"}',
  '{"question":"以下什么兵种克制弓兵","answer":"骑兵"}',
  '{"question":"以下什么兵种克制步兵","answer":"弓兵"}',
  '{"question":"以下什么兵种克制骑兵","answer":"步兵"}',
  '{"question":"以下列哪个国家不属于欧洲","answer":"埃及"}',
  '{"question":"以下哪一个成就是完成别样的烟火成就之前必须要完成的","answer":"千里走单骑"}',
  '{"question":"以下哪一项不是统帅佩拉约拥有的被动技能","answer":"冲锋"}',
  '{"question":"以下哪一项技能不属于统帅熙德","answer":"骑士精神"}',
  '{"question":"以下哪两项均有列奥纳多·达·芬奇创作","answer":"最后的晚餐，蒙娜丽莎"}',
  '{"question":"以下哪个国家不在欧洲","answer":"埃及"}',
  '{"question":"以下哪为统帅参与了赤壁之战","answer":"曹操"}',
  '{"question":"以下哪为统帅擅长率领步兵单位","answer":"理查德一世"}',
  '{"question":"以下哪位是圆桌骑士中的重要成员","answer":"兰斯洛特"}',
  '{"question":"以下哪位皇帝颁布了米兰诏书","answer":"君士坦丁一世"}',
  '{"question":"以下哪位统帅参与了英法百年战争","answer":"贞德"}',
  '{"question":"以下哪位统帅参与了赤壁之战","answer":"曹操"}',
  '{"question":"以下哪位统帅擅长率领弓兵单位","answer":"李成桂"}',
  '{"question":"以下哪位统帅擅长率领步兵单位","answer":"理查一世"}',
  '{"question":"以下哪位统帅擅长率领骑兵单位","answer":"源义经"}',
  '{"question":"以下哪位统帅擅长率领骑兵单位？","answer":"源义经"}',
  '{"question":"以下哪位统帅擅长进攻其他执政官的城市","answer":"尤里乌斯·凯撒"}',
  '{"question":"以下哪位统帅擅长进行资源采集","answer":"克里奥佩特拉七世"}',
  '{"question":"以下哪位统帅是狮心王理查的宿命敌人","answer":"萨拉丁"}',
  '{"question":"以下哪位统帅曾加入反对自己丈夫的叛军中","answer":"科斯坦察"}',
  '{"question":"以下哪位英雄参与了英法百年战争","answer":"贞德"}',
  '{"question":"以下哪种乐器有六根线","answer":"吉他"}',
  '{"question":"以下哪种情况统帅无法获得经验","answer":"击败其他玩家"}',
  '{"question":"以下哪项不是希腊神话的目标","answer":"寻找救济之路"}',
  '{"question":"以下哪项不是文艺复兴艺术作品的特征","answer":"平面/二维"}',
  '{"question":"以下哪项不是文艺复兴运动的支柱之一","answer":"矫饰主义"}',
  '{"question":"以下哪项不是自然资源","answer":"布匹"}',
  '{"question":"以下哪项不能获得联盟个人积分","answer":"给盟友资源援助"}',
  '{"question":"以下哪项为不可再生能源（例如煤炭、石油、天然气）","answer":"化石燃料"}',
  '{"question":"以下哪项可称之为能源","answer":"石油"}',
  '{"question":"以下哪项可称之金属资源","answer":"铁"}',
  '{"question":"以下哪项是可再生资源","answer":"水"}',
  '{"question":"以下哪项是由海洋有机物形成的化石燃料，往往被发现于折迭和倾斜的地层，被用于加热和烹饪？","answer":"天然气"}',
  '{"question":"以下谁为日本源平时代的女将","answer":"巴御前"}',
  '{"question":"以船帆为造型的悉尼标志性建筑是","answer":"悉尼歌剧院"}',
  '{"question":"伽利略的“铁球实验”推翻了哪位古希腊科学家的理论","answer":"亚里士多德"}',
  '{"question":"例如伊拉斯谟这样的欧洲北部人文主义者的主要特点是","answer":"热衷于宗教和社会改革"}',
  '{"question":"俄国历史上的第一位沙皇因为暴政而得到了什么称号","answer":"恐怖的伊凡"}',
  '{"question":"俄国第一位暴政沙皇被称为","answer":"恐怖的伊凡"}',
  '{"question":"俄罗斯最后一位沙皇是","answer":"尼古拉二世"}',
  '{"question":"全程马拉松大约有多少公里","answer":"40公里"}',
  '{"question":"八小时工作制度最早在哪个国家出现","answer":"英国"}',
  '{"question":"公元8世纪至11世纪，欧洲人经常把猖獗的北欧海盗称为什么","answer":"维京人"}',
  '{"question":"公元前6世纪的罗马共和国中有咨询议会职能的是哪个社会机构","answer":"元老院"}',
  '{"question":"利用江河大坝的水产生的能源是","answer":"水能"}',
  '{"question":"利用高温岩浆或地下干热岩石来获取的无尽能源是","answer":"地热能"}',
  '{"question":"勇气圣所带来的增益是","answer":"提升统帅所获经验值"}',
  '{"question":"北极熊的皮肤是什么颜色的","answer":"黑色"}',
  '{"question":"北欧神话中，“永恒之枪”是谁的武器","answer":"奥丁"}',
  '{"question":"印刷机的发明者是","answer":"约翰·古腾堡"}',
  '{"question":"印度皇帝沙贾汗为自己的妻子马哈尔修建了哪座举世无双的建筑","answer":"泰姬陵"}',
  '{"question":"原子的分裂过程是","answer":"裂变"}',
  '{"question":"原子聚合的过程是","answer":"聚变"}',
  '{"question":"变异是指什么发生了改变","answer":"DNA"}',
  '{"question":"变异的重要性","answer":"种群进化"}',
  '{"question":"变异的重要性在于它们能带来","answer":"种族进化所需的基因变种"}',
  '{"question":"古代世界七大奇迹之一，马其顿攻占埃及后修建的灯塔叫什么名字","answer":"亚历山大灯塔"}',
  '{"question":"古代罗马士兵装备是","answer":"自己自备"}',
  '{"question":"古代英国兰开斯特家族和特约克家族为了争夺王位发动了哪场战争","answer":"玫瑰战争"}',
  '{"question":"古埃及历史分为多少个王国","answer":"3个"}',
  '{"question":"古希腊主张官职开放并行公薪制的是哪位民主政治代表人物","answer":"伯里克利"}',
  '{"question":"古希腊哪个城邦的勇士以残酷训练、骁勇善战闻名","answer":"斯巴达"}',
  '{"question":"古希腊哪位思想家喜欢在林荫道上讨论学问被称为“逍遥学派”","answer":"亚里士多德"}',
  '{"question":"古希腊哪城市的勇士训练残酷 骁勇善战","answer":"斯巴达"}',
  '{"question":"古罗马起义领导人“斯巴达克斯”本来是从事什么活动的奴隶","answer":"角斗士"}',
  '{"question":"可以为部队带来攻击力增益的是以下哪种圣地","answer":"烈焰圣坛"}',
  '{"question":"可以从地层中开采出的高价值材料是","answer":"自然资源"}',
  '{"question":"号称中国相声三大发源地的分别是北京天桥天津劝业场和南京的哪里","answer":"夫子庙"}',
  '{"question":"和意大利一样，弗兰德斯地区的文艺复兴时代画家们倾向于创作的作品为","answer":"现实派"}',
  '{"question":"哥伦布的冒险航海是由欧洲哪个国家的国王赞助的","answer":"西班牙"}',
  '{"question":"哪一位英国物理学家于伽利略忌辰出生,于爱因斯坦诞辰去世","answer":"斯蒂芬．霍金"}',
  '{"question":"哪两项著作被认为是荷马创作的","answer":"《奥德赛》和《伊利亚特》"}',
  '{"question":"哪个国家没有参与滑铁卢战役","answer":"意大利"}',
  '{"question":"哪位古希腊的物理学家在洗澡的时候发现了浮力","answer":"阿基米得"}',
  '{"question":"哪位欧洲人撰写了第一部详尽描绘中国历史、文化、艺术的游记","answer":"马可·波罗"}',
  '{"question":"哪位统帅少年时就立志终身与罗马为敌","answer":"汉尼拔·巴卡"}',
  '{"question":"哪位葡萄牙航海家发现了非洲最南端的好望角","answer":"迪亚士"}',
  '{"question":"哪种情形统帅无法获得经验","answer":"击败其他玩家"}',
  '{"question":"哪种自然灾害使用“里氏”来衡量强度","answer":"地震"}',
  '{"question":"哪项发现让象形文字的解读成为可能","answer":"罗塞塔石碑"}',
  '{"question":"唯一一个在大战役中击败居鲁士大帝的帝王是谁?","answer":"托米丽司"}',
  '{"question":"唯一一位以国王的身份被处死的英格兰国王是谁","answer":"查理一世"}',
  '{"question":"国际森林日","answer":"0321"}',
  '{"question":"圣诞老人送礼物时乘坐的雪橇是什么动物拉的","answer":"麋鹿"}',
  '{"question":"在1300至1600的一段时间里，欧洲的历史上的哪项运动引领了传统文化的全新风潮，并对世界的艺术、教育和观念产生了深远影响","answer":"文艺复兴"}',
  '{"question":"在1837年建造了世界上第一台电力机车的发明家是","answer":"戴维森"}',
  '{"question":"在中国十二肖顺序中，排在第一个的是","answer":"鼠"}',
  '{"question":"在公元8世纪的普瓦提埃战役中，展现了天才军事才能的统帅是","answer":"查理.马特"}',
  '{"question":"在古希腊神话中，特洛伊王子帕里斯把写有“给最美的女神“的金苹果给了谁","answer":"阿佛洛狄忒"}',
  '{"question":"在古希腊，《希波克拉底誓言》是哪个职业在如之前需要背诵的","answer":"医生"}',
  '{"question":"在哪一年，狮心王理查和萨拉丁签订了停战协议","answer":"1192年"}',
  '{"question":"在埃及之战中可以使用什么加速道具","answer":"治疗加速"}',
  '{"question":"在埃及之战中，应该将获得的奥西里斯之柩护送到哪里","answer":"非敌方建筑"}',
  '{"question":"在文艺复兴期间，为什么佛罗伦萨如此重要","answer":"文艺复兴的诞生地和标志"}',
  '{"question":"在欧洲中世纪时期左右肆虐的黑死病是指哪种传染病","answer":"鼠疫"}',
  '{"question":"在正常情况下白天用肉眼看到的太阳是它的哪一层","answer":"光球层"}',
  '{"question":"在联盟商店无法购买的物品","answer":"黄金钥匙"}',
  '{"question":"地球大气主要的温室气体中不包含下列哪一项","answer":"氮气"}',
  '{"question":"埃及人造纸的材料是","answer":"纸莎草"}',
  '{"question":"埃及的经济基础是","answer":"农业"}',
  '{"question":"墨西哥国徽上的图案不包括以下哪项","answer":"骆驼"}',
  '{"question":"大部分飓风发源于哪里","answer":"海洋"}',
  '{"question":"太阳系中自传最快的行星是","answer":"木星"}',
  '{"question":"奠定日后欧洲数学基础且著有《图形的分割》的是哪一位古希腊学家","answer":"欧几里得"}',
  '{"question":"奥匈帝国的两个首都分别位于布达佩斯和哪座城市?","answer":"维也纳"}',
  '{"question":"奥运会旗五色环中绿色的环是代表","answer":"大洋洲"}',
  '{"question":"如何获取联盟科技点","answer":"通过在联盟科技中捐献获得"}',
  '{"question":"威尔士在哪位英国国王在位时期被彻底并入了英格兰的管辖范围","answer":"亨利八世"}',
  '{"question":"安息古国遗址现在位于亚洲哪个国家境内","answer":"伊朗"}',
  '{"question":"宙斯的哥哥在希腊神话中为人类带来马匹的是哪位人物","answer":"波塞冬"}',
  '{"question":"小美人鱼雕像位于丹麦哪个城市","answer":"哥本哈根"}',
  '{"question":"市政厅等级最高多少级","answer":"25"}',
  '{"question":"希腊神话中一身三头的地狱看门犬","answer":"刻耳柏洛斯"}',
  '{"question":"希腊神话中，取金羊毛的英雄是谁","answer":"伊阿宋"}',
  '{"question":"希腊神话中，宙斯和赫尔墨斯的关系是","answer":"父子"}',
  '{"question":"希腊神话中，阿喀琉斯的唯一弱点存在于何处","answer":"脚后跟"}',
  '{"question":"建立朝鲜王朝的君主是","answer":"李成桂"}',
  '{"question":"当市政厅达到25级可以修建几个农场","answer":"四个"}',
  '{"question":"影响了欧洲古代史的“布匿战争”，交战双方是古罗马跟哪个古国？","answer":"古迦太基"}',
  '{"question":"德西德里乌斯伊拉斯谟和托马斯摩尔的共同点是什么","answer":"都是人文主义者"}',
  '{"question":"思想者们研究传统文献并关注人类的潜能和成就，这一文艺复兴时期的思想运动是","answer":"人文主义"}',
  '{"question":"意大利文艺复兴的画家主要为哪一派","answer":"现实派"}',
  '{"question":"成吉思汗的蒙古帝国发源自哪里","answer":"亚洲"}',
  '{"question":"扑克牌上红桃K原型是以下哪一位人物？","answer":"查理曼大帝"}',
  '{"question":"托勒密王朝最后一任女法老是谁","answer":"克里奥佩特拉七世"}',
  '{"question":"拉斐尔最著名的作品之一是以下哪项","answer":"雅典学校"}',
  '{"question":"按顺序从数字1写到数字99，总共会写下多少个数字1","answer":"20个"}',
  '{"question":"描述了人类大脑遗忘规律的曲线是由哪位德国心理学家研究发见的","answer":"艾宾浩斯"}',
  '{"question":"提出我思故我在的是哪一位哲学家","answer":"笛卡尔"}',
  '{"question":"文艺复兴时期著名的艺术赞助人洛伦佐公爵来自当时的哪个家族","answer":"美第奇家族"}',
  '{"question":"文艺复兴的起源地是哪里","answer":"意大利"}',
  '{"question":"文艺复兴究竟“复兴”了什么","answer":"传统文化"}',
  '{"question":"文艺复兴起源于意大利的原因不是以下哪项","answer":"阿尔卑斯山脉阻挡了黑死病的蔓延"}',
  '{"question":"无加成的情况下，执政官的行动力上限默认是多少","answer":"1000"}',
  '{"question":"日本战国时代末期杰出的政治家，江户幕府的第一代将军是谁","answer":"德川家康"}',
  '{"question":"日本江户幕府的创始者是谁","answer":"德川家康"}',
  '{"question":"昵称为“乱世枭雄”的统帅是","answer":"曹操"}',
  '{"question":"昵称为“凯尔特玫瑰“的统帅是","answer":"布狄卡"}',
  '{"question":"昵称为“天选之豹“的统帅是","answer":"拜巴尔"}',
  '{"question":"昵称为“死亡蜜酒”的统帅是","answer":"萨尔卡"}',
  '{"question":"昵称为“蛮族咆哮者”的统帅是","answer":"洛哈"}',
  '{"question":"昵称为“迦太基守护者”的统帅是","answer":"汉尼拔·巴卡"}',
  '{"question":"昵称为镰仓战神的统帅是","answer":"源义经"}',
  '{"question":"昵称为麦西亚领主的统帅是","answer":"埃塞尔弗莱德"}',
  '{"question":"最初被认为有毒而无法食用的狼桃是什么水果","answer":"西红柿"}',
  '{"question":"最强执政官活动分为几个阶段","answer":"5个"}',
  '{"question":"最强执政官活动包含以下哪个阶段","answer":"消灭敌军"}',
  '{"question":"月食现象的本质是","answer":"地球刚好移动到太阳与月亮之间"}',
  '{"question":"有些台风形状像是圆锥一样，它是","answer":"旋转的"}',
  '{"question":"有机体适应环境，生存并将其特征遗传给后代的现象有机体为了生存适应环境，并将其特征遗传给后代的现象被称为","answer":"自然选择"}',
  '{"question":"梵蒂冈的\\"圣彼得大教堂\\"是由文艺复兴时期的哪位大师设计","answer":"米开朗基罗"}',
  '{"question":"沼泽地带由植物腐烂形成的沉积岩是","answer":"煤炭"}',
  '{"question":"法兰西第一帝国的皇帝是","answer":"拿破仑·波拿巴"}',
  '{"question":"法国和德国的前身是哪个封建王国","answer":"法兰克王国"}',
  '{"question":"活着为了讲述是哪位诺贝尔文学奖获得者的自传","answer":"马尔克斯"}',
  '{"question":"源义经与谁一起举兵讨伐了平家","answer":"源赖朝"}',
  '{"question":"煤炭,石油,天然气等不可再生能源.以下哪项为不可再生能源","answer":"化石燃料"}',
  '{"question":"牛仔裤中不包括哪种资源","answer":"铅"}',
  '{"question":"牛仔裤成分没有甚么","answer":"铅"}',
  '{"question":"特洛伊战争中，杀死赫克托尔的是希腊联军中的哪位勇士","answer":"阿喀琉斯"}',
  '{"question":"猪科属哺乳纲动物下的哪一个项目","answer":"偶蹄目"}',
  '{"question":"环境适应性从何而来？","answer":"变异和基因重组"}',
  '{"question":"现代奥林匹克之父","answer":"顾拜旦"}',
  '{"question":"现知美索不达米亚文明最早的创造是哪个民族","answer":"苏美尔人"}',
  '{"question":"用于集结联盟部队的建筑","answer":"城堡"}',
  '{"question":"由木材和酒精等有机物燃烧得来的可再生能源是","answer":"生物质能"}',
  '{"question":"由海洋有机物形成的液态化石燃料，可以作为燃料和塑料工业原料的是","answer":"石油"}',
  '{"question":"电流强度单位安和毫安的换算比例是","answer":"1:1000"}',
  '{"question":"目前中国现存最早的兵书是哪一部","answer":"《孙子兵法》"}',
  '{"question":"目前人类已知的最软的矿物是什么","answer":"滑石"}',
  '{"question":"相传特洛伊之战的起因是双方为了争夺哪位美女","answer":"海伦"}',
  '{"question":"眼镜的组成材料包括石英.砂砾和","answer":"石油"}',
  '{"question":"种群中的基因变种越多，部分个体就越有可能","answer":"生存"}',
  '{"question":"种群内部的差异,例如鸟喙的不同,被称作","answer":"变种"}',
  '{"question":"空调的发明者是哪国人","answer":"美国"}',
  '{"question":"第一次十字军东征的集结地是哪里","answer":"君士坦丁堡"}',
  '{"question":"第二次布匿战争中汉尼拔以少胜多大败罗马军队的是哪场战役","answer":"坎尼会战"}',
  '{"question":"约翰·古腾堡的印刷机的有点不包括以下哪项","answer":"提升教会权威"}',
  '{"question":"织田信长为自己定制了一方刻有哪四个字的图章","answer":"天下布武"}',
  '{"question":"经典鸡尾酒血腥玛丽是混和哪种果蔬汁制成的","answer":"番茄汁"}',
  '{"question":"结束金雀花王朝，开启都铎王朝的是哪场战争","answer":"玫瑰战争"}',
  '{"question":"统帅乙支文德擅长的是","answer":"擅长带领步兵"}',
  '{"question":"统帅兰斯洛特擅长擅长的是","answer":"率领骑兵单位"}',
  '{"question":"统帅凯拉擅长的是","answer":"擅长带领弓兵"}',
  '{"question":"统帅凯拉的昵称","answer":"猩红避役"}',
  '{"question":"统帅凯拉的昵称是","answer":"猩红避役"}',
  '{"question":"统帅奥斯曼一世擅长的是","answer":"对其他执政官的城巿发动进攻"}',
  '{"question":"统帅尤里乌斯·凯撒的昵称是","answer":"无冕之王"}',
  '{"question":"统帅汉尼拔巴卡擅长的是","answer":"对别人城市发动进攻"}',
  '{"question":"统帅贝利萨留的昵称是","answer":"最后的罗马人"}',
  '{"question":"维多利亚女王是英国哪个王朝的著名君主","answer":"汉诺威王朝"}',
  '{"question":"美国《独立宣言》的诞生日期是","answer":"1776"}',
  '{"question":"美国独立宣言的诞生日期是","answer":"1776"}',
  '{"question":"美国药剂师约翰彭伯顿发明了哪种饮料","answer":"可乐"}',
  '{"question":"群族基因变种越多 部分个体就越有可能","answer":"生存"}',
  '{"question":"肯德基的创始人是什么军衔","answer":"上校"}',
  '{"question":"自号太阳王的君王是哪一位","answer":"路易十四"}',
  '{"question":"自由女神像是哪个国家送给美国的礼物？","answer":"法国"}',
  '{"question":"英国革命时期国会组织的“新模范军”是由谁负责指挥的","answer":"克伦威尔"}',
  '{"question":"英国革命时期组织的新模范军是由谁负责指挥","answer":"克伦威尔"}',
  '{"question":"荷马史诗的内容与发生在古代历史上的一次战争有关，这场战争是","answer":"特洛伊战争"}',
  '{"question":"莫卧儿帝国将伊斯兰教传播到了","answer":"印度大部分地区"}',
  '{"question":"菲律宾的国花是什么花","answer":"茉莉花"}',
  '{"question":"萨拉丁如何改变了中东局势","answer":"他重新征服了巴勒斯坦大部分地区"}',
  '{"question":"著名卡通形象米老鼠有几根手指","answer":"４根"}',
  '{"question":"蒙古帝国的奠基者是","answer":"孛儿只斤铁木真"}',
  '{"question":"虽然古腾堡是欧洲印刷机的发明者，但发明首个活字印刷技术并影响了他的发明的国家是","answer":"中国"}',
  '{"question":"被印度人称为“圣雄”的是谁","answer":"莫罕达斯·卡拉姆昌德·甘地"}',
  '{"question":"被吉尼斯世界纪录认定为单一作者最大发行量的漫画是","answer":"航海王"}',
  '{"question":"被吉尼斯世界记录认定为“由单一作者创作，累计发行量最大的漫画作品”是哪部漫画","answer":"《航海王》"}',
  '{"question":"被尊称为“圣路易”的是哪位法国国王","answer":"路易九世"}',
  '{"question":"被福尔摩斯称为犯罪界的拿破仑的人是","answer":"莫里亚蒂教授"}',
  '{"question":"被称为“现代奥林匹克之父”的是","answer":"顾拜旦"}',
  '{"question":"被称为“红胡子”的统帅是","answer":"腓特烈一世"}',
  '{"question":"被称为失落之地的马丘比丘是拉美哪种古文明代表","answer":"印加文明"}',
  '{"question":"被誉为加拿大“国宝”的是哪个艺术团体","answer":"太阳马戏团"}',
  '{"question":"谁为文艺复兴提供了主要的经济资助","answer":"富有的赞助人"}',
  '{"question":"谁创作了《最后的晚餐》","answer":"列奥纳多·达·芬奇"}',
  '{"question":"谁创作了《蒙娜丽莎》","answer":"列奥纳多·达·芬奇"}',
  '{"question":"谁制定了《儒略历》","answer":"尤利乌斯·凯撒"}',
  '{"question":"谁在古埃及王国里拥有一切","answer":"法老"}',
  '{"question":"谁建立了阿斯图里亚斯王国","answer":"佩拉约"}',
  '{"question":"谁征服了拜占庭帝国?","answer":"奥斯曼"}',
  '{"question":"谁想出了特洛伊木马的计谋?谁赢得了战争?","answer":"奥德修斯；希腊人"}',
  '{"question":"谁是希腊神话中火神创造出来的第一个女人","answer":"潘多拉"}',
  '{"question":"谁是王国的国王？","answer":"占领神庙的联盟盟主"}',
  '{"question":"谁是罗马帝国第一位皇帝","answer":"盖乌斯·屋大维"}',
  '{"question":"谁是首个统治英格兰的亨利八世子嗣","answer":"爱德华六世"}',
  '{"question":"谁杀了赫克托尔，阿喀琉斯又是怎么死的","answer":"阿喀琉斯；脚跟中箭"}',
  '{"question":"赫克托尔被谁杀_阿基利斯","answer":"阿克流斯"}',
  '{"question":"通过重原子像轻原子的分离来释放能量的能源形式是","answer":"核能"}',
  '{"question":"道具抵抗之箭的用途是","answer":"用于对警戒塔进行升级"}',
  '{"question":"道具盟誓之书的用途是","answer":"用于城堡进行升级"}',
  '{"question":"道具誓盟之书的用途是","answer":"城堡"}',
  '{"question":"酒精会麻痹大脑的哪个部位","answer":"小脑"}',
  '{"question":"金星一天等于地球多少日","answer":"243日"}',
  '{"question":"金酒除了作为饮品以外在18世纪之前还曾用以什么用途","answer":"作为药物"}',
  '{"question":"鎌仓战神","answer":"源义经"}',
  '{"question":"钓起深海鱼放生时为什么要刺穿鱼膘","answer":"为了防止鱼死于气压伤"}',
  '{"question":"阿喀琉斯拒绝出战。普特洛克勒斯穿上了阿喀琉斯的铠甲并在对战特洛伊人时阵亡，谁杀了普特洛克勒斯","answer":"赫克托尔"}',
  '{"question":"阿育王是古印度历史上哪个王朝的国王？","answer":"孔雀王朝"}',
  '{"question":"限制资源使用并保护资源的行为被称作","answer":"资源节约"}',
  '{"question":"青蛙等生物为何能产生大量的卵和后代","answer":"后代数量越多存活几率就越大"}',
  '{"question":"领带“温莎结”因英国哪位国王的喜爱而风靡世界","answer":"爱德华八世"}',
  '{"question":"飓风中心被称为","answer":"眼"}',
  '{"question":"首个活字印刷技术出现在","answer":"中国"}',
  '{"question":"统帅孙武擅长的是?","answer":"在自己的城市中驻守"}',
  '{"question":"万国觉醒本初子午线经过以下哪个国家？","answer":"马里"}'
];

function sort() {
  return data.map((v) => JSON.stringify(v)).sort();
}
