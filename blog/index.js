document.addEventListener('DOMContentLoaded', () => {
  function clearSelectedTheme () {
    document.querySelector('.theme-option-selected').classList.remove('theme-option-selected')
  }
  function setThemePreferToDark () {
    clearSelectedTheme()
    localStorage.setItem('theme-pefers', 'dark')
    document.body.classList.add('dark-theme')
    document.querySelector('.theme-option[data-theme="dark"]').classList.add('theme-option-selected')
  }
  function setThemePreferToLight () {
    clearSelectedTheme()
    localStorage.setItem('theme-pefers', 'light')
    document.body.classList.remove('dark-theme')
    document.querySelector('.theme-option[data-theme="light"]').classList.add('theme-option-selected')
  }
  function setThemePreferToSystem () {
    clearSelectedTheme()
    localStorage.setItem('theme-pefers', 'system')
    const isDarkTheme = matchMedia('(prefers-color-scheme: dark)').matches
    document.querySelector('.theme-option[data-theme="system"]').classList.add('theme-option-selected')
    if (isDarkTheme) {
      document.body.classList.add('dark-theme')
    } else {
      document.body.classList.remove('dark-theme')
    }
  }
  function setThemePerfer () {
    // 初始化颜色主题
    // 获取本地设置
    const localThemeSetting = localStorage.getItem('theme-pefers')
    // 没有本地设置就跟随系统
    if (!localThemeSetting || localThemeSetting === 'system') {
      setThemePreferToSystem()
    } else if (localThemeSetting === 'dark') {
      setThemePreferToDark()
    } else if (localThemeSetting === 'light') {
      setThemePreferToLight()
    }
  }

  // 设置主题偏好
  setThemePerfer()

  // 监听用户切换主题操作
  const $themeOptions = document.querySelectorAll('.theme-option')
  $themeOptions.forEach(($el) => {
    $el.addEventListener('click', (event) => {
      const { theme } = event.target.dataset
      if (theme === 'system') {
        setThemePreferToSystem()
      } else if (theme === 'dark') {
        setThemePreferToDark()
      } else {
        setThemePreferToLight()
      }
    })
  })

  // 监听系统偏好变化
  matchMedia('(prefers-color-scheme: dark)').addEventListener('change', () => {
    setThemePerfer()
  })
})

